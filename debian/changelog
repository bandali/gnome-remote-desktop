gnome-remote-desktop (44.2-1) UNRELEASED; urgency=medium

  * New upstream release
  * Update standards version to 4.6.2, no changes needed

 -- Amin Bandali <bandali@ubuntu.com>  Mon, 29 May 2023 14:19:05 -0400

gnome-remote-desktop (44.1-1) experimental; urgency=medium

  * New upstream release
  * debian/upstream/metadata: Fix malformed file, causing gbp clone with
    --add-upstream-vcs to crash with a traceback with the following error
    at the end:
      yaml.scanner.ScannerError: mapping values are not allowed here
      in "debian/upstream/metadata", line 2, column 13

 -- Amin Bandali <bandali@ubuntu.com>  Thu, 27 Apr 2023 11:38:46 -0400

gnome-remote-desktop (44.0-1) experimental; urgency=medium

  * New upstream release

 -- Jeremy Bicha <jbicha@ubuntu.com>  Mon, 27 Mar 2023 08:42:38 -0400

gnome-remote-desktop (44~rc-1) experimental; urgency=medium

  * New upstream release
  * Depend on libmutter-12 instead of libmutter-11
  * debian/control.in: Bump minimum freerdp to 2.10.10

 -- Jeremy Bicha <jbicha@ubuntu.com>  Wed, 15 Mar 2023 09:30:27 -0400

gnome-remote-desktop (43.3-1) unstable; urgency=medium

  [ Jeremy Bicha ]
  * New upstream release
  * Remove incorrect nocheck annotation from libgbm-dev Build-Depends
    (Closes: #1023582)

  [ Ben Westover ]
  * Update dependencies and description to reflect removal of VNC

 -- Jeremy Bicha <jbicha@ubuntu.com>  Tue, 17 Jan 2023 11:16:08 -0500

gnome-remote-desktop (43.2-1) unstable; urgency=medium

  * New upstream release (LP: #1995245)

 -- Jeremy Bicha <jbicha@ubuntu.com>  Tue, 06 Dec 2022 14:49:34 -0500

gnome-remote-desktop (43.1-1) unstable; urgency=medium

  * New upstream release (LP: #1995245)
  * Drop all patches: applied in new release

 -- Jeremy Bicha <jbicha@ubuntu.com>  Mon, 31 Oct 2022 10:38:58 +0100

gnome-remote-desktop (43.0-2) unstable; urgency=medium

  * Update mutter dependency for mutter 43

 -- Jeremy Bicha <jbicha@ubuntu.com>  Tue, 27 Sep 2022 19:25:16 -0400

gnome-remote-desktop (43.0-1) unstable; urgency=medium

  * New upstream release (Closes: #1019342)
    - Only RDP is provided now because it offers better performance and
      privacy and is supported by a wide number of clients (Closes: #1012271)
  * Don't build audio forwarding feature since fdk-aac isn't in Debian
    or Ubuntu 'main' yet
  * debian/control.in: Build-Depend on libtss2-dev
  * debian/control.in: Build-Depend on asciidoc-base & docbook-xml
  * debian/control.in: Bump minimum freerdp to 2.8.0
  * debian/control.in: Set Rules-Requires-Root: no
  * Cherry-pick 2 patches to fix crashes

 -- Jeremy Bicha <jbicha@ubuntu.com>  Wed, 21 Sep 2022 19:45:03 -0400

gnome-remote-desktop (42.4-1) unstable; urgency=medium

  * New upstream release (LP: #1983788)

 -- Jeremy Bicha <jbicha@ubuntu.com>  Sun, 07 Aug 2022 16:46:54 -0400

gnome-remote-desktop (42.3-1) unstable; urgency=medium

  * Team upload
  * New upstream release (LP: #1980748)

 -- Nathan Pratta Teodosio <nathan.teodosio@canonical.com>  Mon, 04 Jul 2022 11:36:33 -0300

gnome-remote-desktop (42.2-1) unstable; urgency=medium

  * New upstream release (LP: #1976547)

 -- Jeremy Bicha <jbicha@ubuntu.com>  Wed, 01 Jun 2022 14:54:51 -0400

gnome-remote-desktop (42.1.1-2) unstable; urgency=high

  * Don't automatically enable the systemd user service (LP: #1973028)
  * Add postinst to remove the automatic enabling of the user service
  * debian/control: Lower mutter dependency so that these fixes reach
    Testing sooner

 -- Jeremy Bicha <jbicha@ubuntu.com>  Thu, 12 May 2022 15:19:46 -0400

gnome-remote-desktop (42.1.1-1) unstable; urgency=medium

  * New upstream release (LP: #1970662)
    - Fixes black screen with virtio on qemu (LP: #1971195)
  * Drop all patches: applied in new release
  * Depend on libmutter instead of gnome-shell | budgie-desktop
    - This is a more accurate dependency
  * Require libmutter 42.1 for Nvidia fixes
  * Depend on fuse3 (Closes: #998846) (LP: #1970411)

 -- Jeremy Bicha <jbicha@ubuntu.com>  Tue, 10 May 2022 14:50:48 -0400

gnome-remote-desktop (42.0-4) unstable; urgency=medium

  * debian/patches: Ensure support with nvidia drivers >= 495
    the NVIDIA >= 495 drivers apparently allows creating and importing dma-bufs
    However, this won't succeed, but without an error.
    The result is that undefined content is drawn onto the screen on clients.
    This affects both the RDP and VNC backend.
    Disabling dma-bufs in such case gets rid of this issue, because with this
    patch g-r-d always assumes, that dma-bufs are unavailable with the NVIDIA
    driver.

 -- Marco Trevisan (Treviño) <marco@ubuntu.com>  Thu, 14 Apr 2022 12:59:26 +0200

gnome-remote-desktop (42.0-3) unstable; urgency=medium

  * Team upload
  * Add a Breaks to get -control-center and -remote-desktop upgraded together
  * Release to unstable

 -- Simon McVittie <smcv@debian.org>  Wed, 13 Apr 2022 16:49:06 +0100

gnome-remote-desktop (42.0-2) experimental; urgency=medium

  * Cherry-pick commit to autostart as part of gnome-session.target

 -- Jeremy Bicha <jbicha@ubuntu.com>  Tue, 12 Apr 2022 08:22:43 -0400

gnome-remote-desktop (42.0-1) experimental; urgency=medium

  * New upstream release

 -- Jeremy Bicha <jbicha@ubuntu.com>  Mon, 21 Mar 2022 08:22:43 -0400

gnome-remote-desktop (42~rc-1) experimental; urgency=medium

  * New upstream release
  * debian/control.in: Bump minimum libffmpeg-nvenc-dev to 11.1.5

 -- Jeremy Bicha <jeremy.bicha@canonical.com>  Tue, 08 Mar 2022 10:45:32 -0500

gnome-remote-desktop (42~beta-1) experimental; urgency=medium

  * New upstream release
  * debian/control.in: Bump minimum freerdp to 2.5.0
  * debian/control.in: Build-Depend on libdrm-dev & libepoxy-dev
  * debian/control.in: Build-Depend on libgbm-dev & libgudev-1.0-dev for tests

 -- Jeremy Bicha <jeremy.bicha@canonical.com>  Wed, 16 Feb 2022 08:41:30 -0500

gnome-remote-desktop (41.2-1) unstable; urgency=medium

  * New upstream release

 -- Jeremy Bicha <jbicha@debian.org>  Fri, 14 Jan 2022 07:11:05 -0500

gnome-remote-desktop (41.1-3) unstable; urgency=medium

  * debian/control.in:
    - Depends on pipewire-media-session | wireplumber, without one of those
      the video streams are paused and sharing isn't working

 -- Sebastien Bacher <seb128@ubuntu.com>  Wed, 01 Dec 2021 13:15:36 +0100

gnome-remote-desktop (41.1-2) unstable; urgency=medium

  * Build-Depend on libffmpeg-nvenc-dev & enable NVENC support

 -- Jeremy Bicha <jbicha@debian.org>  Mon, 22 Nov 2021 19:57:17 -0500

gnome-remote-desktop (41.1-1) unstable; urgency=medium

  * New upstream release
  * d/p/u/meson-Add-option-to-disable-rdp-clipboard-support.patch,
    debian/rules:
    - remove hacks to build without RDP clipboard on Ubuntu since fuse3
      is going to be promoted now.

 -- Sebastien Bacher <seb128@ubuntu.com>  Mon, 08 Nov 2021 12:10:27 +0100

gnome-remote-desktop (41.0-1) unstable; urgency=medium

  * New upstream release
  * Rebase rdp-clipboard option patch
  * debian/control.in: Disable nvenc; needs new libffmpeg-nvenc-dev

 -- Jeremy Bicha <jbicha@debian.org>  Mon, 27 Sep 2021 17:24:20 -0400

gnome-remote-desktop (40.2-1) unstable; urgency=medium

  * New upstream release
  * Drop bash patch since similar fix was applied in new release
  * debian/rules: Only disable RDP clipboard on Ubuntu, not derivatives
  * Bump debhelper-compat to 13
  * Bump Standards-Version to 4.6.0

 -- Jeremy Bicha <jbicha@debian.org>  Sat, 18 Sep 2021 22:07:18 -0400

gnome-remote-desktop (40.1-4) unstable; urgency=medium

  * Team upload
  * Release to unstable (transition: #992870)

 -- Simon McVittie <smcv@debian.org>  Sat, 11 Sep 2021 21:47:36 +0100

gnome-remote-desktop (40.1-3) experimental; urgency=medium

  * Drop direct dependency on libmutter
  * Build-Depend on dh-sequence-gnome instead of gnome-pkg-tools

 -- Jeremy Bicha <jbicha@debian.org>  Mon, 19 Jul 2021 07:19:03 -0400

gnome-remote-desktop (40.1-2) experimental; urgency=medium

  * debian/patches: Fix a typo causing RDP clipboard not to be enabled in debian

 -- Marco Trevisan (Treviño) <marco@ubuntu.com>  Tue, 01 Jun 2021 13:04:42 +0200

gnome-remote-desktop (40.1-1) experimental; urgency=medium

  * New upstream release:
  * debian/control:
    - Update build-dependencies to follow upstream
    - Also depend on any libmutter-8 version
  * debian/patches:
    - Remove patches merged upstream
    - Add option to disable rdp clipboard support.
      This requires libfuse-3 which is not in Ubuntu main yet, so add an
      option to disable it
  * debian/rules: Disable RDP clipboard in ubuntu builds.
    RDP clipboard requires libfuse-3 that is not in ubuntu main yet

 -- Marco Trevisan (Treviño) <marco@ubuntu.com>  Mon, 31 May 2021 18:03:34 +0200

gnome-remote-desktop (0.1.9-5) unstable; urgency=medium

  * Team upload

  [ David Mohammed ]
  * debian/control: add budgie-desktop as an alternate for gnome-shell
    (Closes: #982937)

 -- Simon McVittie <smcv@debian.org>  Tue, 09 Mar 2021 10:31:04 +0000

gnome-remote-desktop (0.1.9-4) unstable; urgency=medium

  * debian/patches: Fix use-after-free crash on repeated VNC connections

 -- Marco Trevisan (Treviño) <marco@ubuntu.com>  Thu, 11 Feb 2021 17:14:14 +0100

gnome-remote-desktop (0.1.9-3) unstable; urgency=medium

  * Team upload
  * Add patch from upstream, via Fedora, to fix crashes
  * Add patch to make test script find bash on non-merged-/usr systems
  * Standards-Version: 4.5.1 (no changes required)
  * Simplify dependencies now that GNOME 3.38 is in unstable

 -- Simon McVittie <smcv@debian.org>  Sun, 17 Jan 2021 13:53:19 +0000

gnome-remote-desktop (0.1.9-2) unstable; urgency=medium

  * Team upload
  * Relax runtime dependency to be satisfiable in unstable
  * Upload to unstable for PipeWire 0.3 transition (Closes: #966521)

 -- Simon McVittie <smcv@debian.org>  Thu, 10 Sep 2020 14:04:30 +0100

gnome-remote-desktop (0.1.9-1) experimental; urgency=medium

  * Team upload
  * New upstream release
  * d/upstream/metadata: Add.
    The canonical upstream git repo has moved to
    <https://gitlab.gnome.org/GNOME/gnome-remote-desktop>.
  * d/watch, d/copyright: Switch source of releases.
    Newer versions of this package are now available from
    <https://download.gnome.org/sources/gnome-remote-desktop/>.
  * d/copyright: Update
  * d/control.in: Update dependencies for new RDP support
  * d/control.in: Depend on a compatible version of GNOME Shell

 -- Simon McVittie <smcv@debian.org>  Wed, 09 Sep 2020 12:38:19 +0100

gnome-remote-desktop (0.1.8-1) experimental; urgency=medium

  * Team upload.

  [ Laurent Bigonville ]
  * New upstream release
    - debian/control.in: Bump the build-dependencies

  [ Andreas Henriksson ]
  * Use user-unit dir from systemd pkg-config file
  * Make tests non-fatal for now

 -- Andreas Henriksson <andreas@fatal.se>  Sun, 06 Sep 2020 23:43:23 +0200

gnome-remote-desktop (0.1.7-2) unstable; urgency=medium

  * Bump debhelper compatibility to 12
  * Bump Standards-Version to 4.5.0 (no further changes)
  * Move the daemon to /usr/libexec now that's allowed in the policy

 -- Laurent Bigonville <bigon@debian.org>  Sat, 21 Mar 2020 11:44:15 +0100

gnome-remote-desktop (0.1.7-1) unstable; urgency=medium

  * New upstream release

 -- Laurent Bigonville <bigon@debian.org>  Thu, 21 Feb 2019 10:53:14 +0100

gnome-remote-desktop (0.1.6-2) unstable; urgency=medium

  * Add -Wl,-O1 -Wl,-z,defs -Wl,--as-needed to our LDFLAGS
  * Enable all hardening flags
  * Bump Standards-Version to 4.3.0

 -- Jeremy Bicha <jbicha@debian.org>  Tue, 25 Dec 2018 08:50:19 -0500

gnome-remote-desktop (0.1.6-1) unstable; urgency=medium

  * Initial release. Closes: #909670

 -- Jeremy Bicha <jbicha@debian.org>  Thu, 27 Sep 2018 01:05:41 -0400
